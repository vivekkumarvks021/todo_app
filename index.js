const express=require('express');
const app=new express();

var todo_router = require('./router/router');
var bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/todo', ()=>{
    console.log("Database connected")
})

app.use('/',todo_router);
app.listen(3000,()=>{
    console.log('server running on 3000')
})