var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Todo_Schema = new Schema({
    name: {
        type: String,
        Required: 'Kindly enter the name of the task'
    },
    designation: {
        type: String,
        default: 'Engineer'
    },
    role: {
        type: String,
        default:'Developer'
    }
});

module.exports = mongoose.model('Todo_data', Todo_Schema);