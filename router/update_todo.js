var todo_data = require('../model/todo')


exports.update_todo = (req,res,next)=>{
    if(!req.body.name){
        res.json({
            success: false,
            msg: "Please provide name"
        })
    }else{
        
           var data = {name: req.body.name};
           var new_data ={
            designation: req.body.designation,
            role: req.body.role
           };
            new_data={$set : new_data}

        todo_data.findOneAndUpdate(data,new_data,(err, data)=>{
            if(err){
                next();
            }else{
                res.json({
                    success: true,
                    msg: "data is updated"
                })
            }
        })
    }
}