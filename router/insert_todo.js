var todo_data = require('../model/todo')


exports.insert_todo = (req,res,next)=>{
    if(!req.body.name){
        res.json({
            success: false,
            msg: "Please provide name"
        })
    }else{
        var data_of_todo = new todo_data({
            name: req.body.name,
            designation: req.body.designation,
            role: req.body.role
        })
        data_of_todo.save((err, data)=>{
            if(err){
                next();
            }else{
                res.json({
                    success: true,
                    msg: "data is inserted into todo document"
                })
            }
        })
    }
}