var todo_data = require('../model/todo')


exports.remove_todo = (req,res,next)=>{
    if(!req.body.name){
        res.json({
            success: false,
            msg: "Please provide name"
        })
    }else{
        
        var data = {name: req.body.name};
        
        todo_data.findOneAndRemove(data,(err, data)=>{
            if(err){
                next();
            }else{
                res.json({
                    success: true,
                    msg: "document is removed"
                })
            }
        })
    }
}