var express = require('express')
var router =express.Router();


//var createTodo = require('./creatTodo')
//router.post('/createTodo',tokenVerify.tokenVerify,createTodo.createTodo)

//var registration = require('./registration')
//router.post('/register', registration.registration)

var insert_todo = require('./insert_todo')
router.post('/insert_todo', insert_todo.insert_todo);

var show_todo = require('./show_todo')
router.post('/show_todo',show_todo.show_todo);

var update_todo = require('./update_todo')
router.post('/update_todo',update_todo.update_todo);

var remove_todo = require('./remove_todo')
router.post('/remove_todo',remove_todo.remove_todo);

module.exports = router;